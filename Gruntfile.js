module.exports = function(grunt) {

	grunt.initConfig({

		//our JSHint options
		jshint : {
			all : ['client/js/', 'common'] //files to lint
		},

		//our concat options
		concat : {
			options : {
				separator : ';' //separates scripts
			},
			dist : {
				src : ['js/*.js', 'js/**/*.js'], //Using mini match for your scripts to concatenate
				dest : 'js/script.js' //where to output the script
			}
		},

		//our uglify options
		uglify : {
			js : {
				files : {
					'js/script.js' : ['js/script.js'] //save over the newly created script
				}
			}
		},

		jsdoc : {
			dist : {
				src : ['client/js/*.js', 'client/js/**/*.js', 'common/*.js'],
				options : {
					destination : 'doc',
					configure : 'node_modules/angular-jsdoc/common/conf.json',
					template : 'node_modules/angular-jsdoc/angular-template',
					tutorial : 'tutorials',
					readme : './README.md'
				}
			}
		},

		mochaTest : {
			test : {
				options : {
					reporter : 'spec',
					captureFile : 'results.txt', // Optionally capture the reporter output to a file
					quiet : false, // Optionally suppress output to standard out (defaults to false)
					clearRequireCache : false // Optionally clear the require cache before running tests (defaults to false)
				},
				src : ['test/**/*.js']
			}
		}
	});

	//load our tasks
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-jsdoc');
	grunt.loadNpmTasks('grunt-mocha-test');

	grunt.registerTask('development', ['jshint', 'mochaTest']);
	grunt.registerTask('production', ['jshint', 'concat', 'uglify']);
};
