# Auction

This project is a small test for NodeJS.

I did not have time to make all this example running, but I made the initial flow and used all the tools to make build, code quality, small tests and docs.

All this work was done in 4 hours.


# Installation

To install the backend libs, used by node, we use NPM. So just do:

* npm install

To install the frontend libs we use bower, so just use:

* bower install


## Database

Install a MYSQL server and a database called auction.

To initialize the database just run the file server/db.txt

## Tools

* npm install angular-jsdoc
* npm install mocha
* npm install jshint
* npm install jsdoc
* npm install grunt
* npm install grunt-cli
* npm install grunt-contrib-jshint grunt-contrib-concat grunt-contrib-uglify
* npm install grunt-jsdoc
* npm install marked
* npm install grunt-mocha-test

# Run

To run the app use supervisor or node directly, here are the examples

* supervisor .
* node .

## Code Quality

To check the code quality we use JSHint as a grunt task:

* grunt jshint

## Docs

To make documentation we have JSDocs and to make all docs run:

* grunt jsdoc

The genereted documents are in folder:

* /auction/doc/index.html

## Test

All tests uses Mocha framework and to run the tests use:

* grunt mochaTest
