/*

*/
var urlbaseAPI='/api';

/**
 * Start the client app using Angular and all modules
 * 
 * @namespace angular_module
 * @summary: This is the main script where starts the Angular App
 */
var app = angular.module('auction', [
	'lbServices',
	'ui.router',
   'ui.router',
   'ngAnimate', 
   'ui.bootstrap',
   'angular-loading-bar',
   'auction.services',
   'timer'
 ])
  .config(function(LoopBackResourceProvider) {
 
    // Use a custom auth header instead of the default 'Authorization'
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');
 
    // Change the URL where to access the LoopBack REST API server
    LoopBackResourceProvider.setUrlBase(urlbaseAPI);
    // Tested: //web.netexames.com.br/api as shows here https://apidocs.strongloop.com/loopback-sdk-angular/#loopbackresourceprovider-seturlbase
  })
  
  /**
   * Configure all routes used in client side
   */
 .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
     $urlRouterProvider) {
   
   $stateProvider
    
    
    .state('login', {
       url: '/login',
       templateUrl: 'js/auction/templates/login.html',
       controller: 'LoginCtrl'
     })
      .state('auction', {
       url: '/auction',
       templateUrl: 'js/auction/templates/auction.html',
       controller: 'AuctionCtrl'
     })
     
     
     

     
         
      ;
   $urlRouterProvider.otherwise('login');
 }]);
 

  
app.run(function ($rootScope,$location) {
    
     
});


/**
 * Configure the loading bar for client
 */
app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	//cfpLoadingBarProvider.includeSpinner = false; cfpLoadingBarProvider.includeBar = false;
	cfpLoadingBarProvider.spinnerTemplate = '<div id="mydiv"><img src="/img/loading.gif" class="ajax-loader"/></div>';
}]);


//Initialize the client services
var auctionServices = angular.module('auction.services', []);
