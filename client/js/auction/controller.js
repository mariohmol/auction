/**
  * @class angular_module.Auction.LoginCtrl
  * @name LoginCtrl
  * @memberOf angular_module
  * @description Makes the user login or create when it does not exists
  */
app.controller('LoginCtrl', function($scope, $rootScope, $state, $stateParams, $location, $filter, User) {


	/**
	  * @name $scope.login
	  * @memberOf angular_module.Auction.LoginCtrl
	  * @description Makes the user login or create when it does not exists
	  */
	$scope.login = function() {

		$scope.usuario.email = $scope.usuario.username + "@rama.net.br";
		$scope.usuario.password = $scope.usuario.username;

		User.login($scope.usuario).$promise.then(function(response) {
			$rootScope.currentUser = {
				id : response.user.id,
				tokenId : response.id,
				email : $scope.email
			};
			console.log(response);
			$scope.initSession(response.user);
			$location.path("/auction");

		}, function errorCallback(response) {

			User.create($scope.usuario).$promise.then(function(response) {
				console.log(response);
				$scope.initSession(response);
				$location.path("/auction");
			});

		});

	};


	/**
	  * @name $scope.initSession
	  * @memberOf angular_module.Auction.LoginCtrl
	  * @description Initialize the local session for the logged user
	  */
	$scope.initSession = function(usuario) {
		sessionStorage.userid = usuario.id;
		sessionStorage.usercoins = usuario.coins;
		sessionStorage.username = usuario.username;
	};

}).controller('AuctionCtrl', function($scope, $rootScope, $state, $location, $controller, $uibModal, User, Inventory, Auction,AuctionService) {

	$scope.userid = sessionStorage.userid;
	$scope.usercoins = sessionStorage.usercoins;
	$scope.username = sessionStorage.username;	


	$scope.init = function(){
		 $scope.timerRunning = false;
		 $scope.auction=null;
		Inventory.find({
			filter : {
				where : {
					userid : $scope.userid
				}
			}
		}).$promise.then(function(response) {
			$scope.inventories = response;
		});
		
		
		AuctionService.findAuction().then(function(resp){
			if(resp && resp.end>new Date()){
				console.log(resp);
				$scope.auction=resp;
				if($scope.auction.end>new Date())$scope.timerRunning = true;
				$scope.bidwin = $scope.auction.bidwin+1;
			}
				
		});
	};
	
	
	

	$scope.logout = function() {
		$location.path("/login");
		sessionStorage.userid = null;
		sessionStorage.usercoins = null;
		sessionStorage.username = null;
	};

	$scope.newModal = function(item) {
		
		AuctionService.findAuction().then(function(resp){
			if(resp)
			alert('There is a auction already open!');
			else{
				var modalInstance = $uibModal.open({
					animation : $scope.animationsEnabled,
					templateUrl : 'js/auction/templates/new.html',
					controller : 'NewModalInstanceCtrl',
					size : 'md',
					resolve : {
						item : item
					}
				});
		
				modalInstance.result.then(function(auction) {
					auction.userid=$scope.userid;
					Auction.create(auction).$promise.then(function(resp){
						$scope.init();
					});
				}, function() {});
			}
				$scope.auction=resp;
		});
	};
	
	$scope.placebid = function() {
		if($scope.auction){
			$scope.auction.bidwin = $scope.bidwin;
			$scope.auction.userwin = $scope.userid;
			console.log($scope.auction);
			Auction.upsert($scope.auction).$promise.then(function(resp){
				alert("Big accepted!");
				$scope.init();
			});
			
		}
		
		
	};
	
	$scope.init();
	
 
    $scope.startTimer = function (){
        $scope.$broadcast('timer-start');
        $scope.timerRunning = true;
    };
 
    $scope.stopTimer = function (){
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = false;
    };
 
    $scope.$on('timer-stopped', function (event, data){
    	$scope.init();
        console.log('Timer Stopped - data = ', data);
    });


})



.controller('NewModalInstanceCtrl', function ($scope, $uibModalInstance,Auction, item) {

	$scope.item=item;
	
	$scope.auction=new Auction();

  $scope.ok = function () {
    $uibModalInstance.close($scope.auction);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})



; 