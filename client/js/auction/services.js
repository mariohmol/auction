/**
  * @ngdoc factory
  * @class angular_module.Auction.AuctionService
  * @name AuctionService
  * @memberOf angular_module
  * @description This is angularjs service for all auction functions
  */
app.factory('AuctionService', function($q,Auction,User,Inventory) {
		
    	return {
    		
    		timeauction: function(){
    			return 90;
    		},
    		/**
			  * @function findAuction
			  * @memberOf angular_module.AuctionService
			  * @description This is find a active auction or active one that isnt started
			  * @return Promise that resolves the active auction
			  */
    			
    		findAuction: function(id){
    			var deferred = $q.defer();
    			var service = this;
    			//gt
    			
    			endtime=new Date();
				endtime.setSeconds(endtime.getSeconds() - service.timeauction());
				Auction.find({filter: {where: {end: {gt: endtime}}, include: ["Inventory","user"]}}).$promise.then(function(resp){
					if(resp && resp.length>0)  {
						service.initAuction(resp[0]).then(function(resp){
							deferred.resolve(resp);
						});
						
					}else{
						service.closeEmpty().then(function(resp){
							filtro.filter.where.start =null;
							Auction.find({filter: {where: {start: null}}, include: ["Inventory","user"]}).$promise.then(function(resp){
								if(resp && resp.length>0)  {
									service.initAuction(resp[0]).then(function(resp){
										deferred.resolve(resp);
									});
								}else deferred.resolve(null);
							});
						});
						
						
					}
				});
				
				return deferred.promise;
    		},
    		
    		
    		/**
			  * @function closeEmpty
			  * @memberOf angular_module.AuctionService
			  * @description Close any auction that started but has not ended
			  * @return Null
			  */
    		closeEmpty: function(){
    			var deferred = $q.defer();
    			var service = this;
    			
    			
    			endtime=new Date();
				endtime.setSeconds(endtime.getSeconds() + service.timeauction());
    			filtro = {filter: {where: {and: [{end: null}]} , include: ["Inventory","user"] }};
    			Auction.find(filtro).$promise.then(function(resp){
					if(resp && resp.length>0)  {
						auc=resp[0];
						auc.end=new Date();
						
						service.updateBuyer(auc).then(function(resp){
							service.updateSeller(auc).then(function(resp){
								Auction.upsert(auc).$promise.then(function(resp){
									deferred.resolve(true);
								});
							});
						});
						
					}else deferred.resolve(true);
				});	
				return deferred.promise;
    		},
    		
    		updateBuyer: function(auc){
    			var deferred = $q.defer();
    			
    			User.find({filter: {where: {id: auc.userwin}}}).$promise.then(function(resp){
					if(resp && resp.length>0){
						usuario=resp[0];
						usuario.coins=usuario.coins-auc.bidwin;
						User.upsert(usuario);
						if(auc.Inventory)
							Inventory.find({filter: {where: {and: [{userid: usuario.id}, {itemname: auc.Inventory.itemname}  ]} }} ).$promise.then(function(resp){
								if(resp && resp.length>0){
									inv=resp[0];
									inv.quantity=inv.quantity-auc.quantity;
									Inventory.upsert(inv);
								}
								deferred.resolve(true);
							});
						else deferred.resolve(true);
					}else deferred.resolve(true);
				});
				
				return deferred.promise;
    			
    		},
    		
    		updateSeller: function(){
    			var deferred = $q.defer();
    			
    			User.find({filter: {where: {id: auc.userid}}}).$promise.then(function(resp){
					if(resp && resp.length>0){
						usuario=resp[0];
						usuario.coins=usuario.coins+auc.bidwin;
						User.upsert(usuario);
						if(auc.Inventory)
							Inventory.find({filter: {where: {and: [{userid: usuario.id}, {itemname: auc.Inventory.itemname}  ]} }} ).$promise.then(function(resp){
								if(resp && resp.length>0){
									inv=resp[0];
									inv.quantity=inv.quantity+auc.quantity;
									Inventory.upsert(inv);
								}
								deferred.resolve(true);
							});
						else deferred.resolve(true);
					}else deferred.resolve(true);
				});
				
				return deferred.promise;
    		},
    		
    	
    	
    	
    	/**
		  * @function initAuction
		  * @memberOf angular_module.AuctionService
		  * @description This prepare a auction including the value of a minim bid and a expected end, considering 90 seconds
		  */
	    	initAuction: function(auction){
	    		var service=this;
	    		var deferred = $q.defer();
	    		if(!auction.start){
	    			auction.start=new Date();
					Auction.upsert(auction).$promise.then(function(resp){
						deferred.resolve(service.prepareAuction(auction));
					});
	    		}else if(!auction.end){
	    			service.closeEmpty().then(function(resp){
						deferred.resolve(service.prepareAuction(resp));
					});
	    		}
	    		
	    		else deferred.resolve(service.prepareAuction(auction));
	    		return deferred.promise;
	    	},

		
    	
    	
    	/**
		  * @function prepareAuction
		  * @memberOf angular_module.AuctionService
		  * @description This prepare a auction including the value of a minim bid and a expected end, considering 90 seconds
		  */
	    	prepareAuction: function(auction){
	    		
	    		if(auction.bidwin) auction.min = auction.bidwin+1;
	    		else auction.min = auction.bidmin;
	    		
	    		auction.end = new Date(auction.start);
	    		auction.end.setSeconds(auction.end.getSeconds() + this.timeauction());
	    		return auction;
	    	}

		};
    });