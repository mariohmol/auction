var loopback = require('loopback');
var config = require('../../server/config.json');
var path =require('path');


/**
  * @class node_models.user
  * @name user
  * @memberOf node_models
  * @description This is the mapped user model from database
  */
module.exports = function(user) {
	
    
	/**
	  * @name user.afterRemote_create
	  * @memberOf node_models
	  * @description After a new user is created, includes the base 3 inventory items and 1000 coins
	  */
  user.afterRemote('create', function(context, usuario, next) {
    console.log('> user.afterRemote triggered -------------');
    
    
    var models = user.app.models;
    
    //A new player starts with a balance of 1000 coins, and at the inventory: 30 breads, 18 carrots and 1 diamond.
    
    models.Inventory.create({itemname: "breads", userid: usuario.id, quantity: 30}, function(err, respuser) {
    	
    	console.log(err);
    	
    	
    	models.Inventory.create({itemname: "carrots", userid: usuario.id, quantity: 18});
    	models.Inventory.create({itemname: "diamond", userid: usuario.id, quantity: 1});
    });
    
    usuario.coins=1000;
    next(null, usuario);
    
  });
  

};
